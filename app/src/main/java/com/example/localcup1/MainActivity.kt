package com.example.localcup1

import android.content.Context
import android.icu.text.CaseMap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

fun error(context:Context, title:String,message:String){
    AlertDialog.Builder(context,)
        .setTitle(title)
        .setMessage(message)
        .setNegativeButton("Ok",null)
        .show()
}
var token = ""

val api1 = retrofit1.create(API::class.java)
val api2 = retrofit2.create(API2::class.java)

val retrofit1 = Retrofit.Builder().baseUrl("").addConverterFactory(GsonConverterFactory.create()).build()
val retrofit2 = Retrofit.Builder().baseUrl("").addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build()
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener{
val Rmail = EditMail.text
        if (Rmail.isBlank()){
            error(this,"Error","Wrong Mail")
        }
        val Rpass = EditPass.text
        if(Rpass.isBlank()){
            error(this,"Error","Wrong Password")
        }
        val Rlogin = EditLogin.text
        if(Rlogin.isBlank()){
            error(this,"Error","Wrong Login")
        }}

    }
}