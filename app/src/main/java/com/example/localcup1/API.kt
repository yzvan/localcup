package com.example.localcup1

import android.media.session.MediaSession
import retrofit2.http.Body
import retrofit2.http.POST

interface API {
    @POST("SignUp")
    fun reg(@Body body: RegB):retrofit2.Call<String>
    @POST("SignIn")
    fun log(@Body body:LogB):retrofit2.Call<Token>
}